asdasd![https://www.utec.edu.pe/sites/default/files/logo_0.png](http://www.utec.edu.pe/sites/default/files/logo_utec_.png)

Exampleeee
# Prueba Tecnica Frontend

El objetivo de esta evaluación es validar los skills de desarrollo que el area solicita.

# Pre requisitos

* Tener instalado npm y nodejs
* Tener instalado un cliente GIT
* Tener una cuenta en bitbucket
* Tener instalado node y npm
* Api para pruebas : https://github.com/jrichardsz/nodejs-express-snippets/blob/master/05-ui-api.js

# Instrucciones

- Se solicitará desarrollar un solo formulario que cumpla con ciertas especificaciones.
- La prueba tiene una duración de 60 minutos.

# Especificaciones del formulario web

# PARTE 01

Se solicita desarrollar un formulario web usando react, angular o vue de forma dinamica, usando el siguiente json para crear el los componentes html

```json
{
  "idForm": "F105",
  "description": "Fix data #105",
  "submitEndpoint": "http://localhost:5000/submit",
  "parameters": [
    {
      "key": "name",
      "label": "nombre del empleado",
      "type": "input"
    },
    {
      "key": "lastname",
      "label": "apellido del empleado",
      "type": "input"
    },
    {
      "key": "job",
      "label": "trabajo del empleado",
      "type": "textArea"
    }
  ]
}
```

# PARTE 02

Agregar un boton submit al formulario anterior y enviar los datos del formulario usando ajax:

```json
{
 "idForm": "F105",
 "parameters" : [
   {
     "key":"name",
     "value":"john"
   },
   {
     "key":"lastname",
     "value":"wick"
   },
   {
     "key":"job",
     "value":"murder"
   }
 ]
}
```

# Criterios de evaluacion


* Uso de algun framework js como angular, react o vue (ultimas versiones)
* Correcto uso del package.json
* Calidad del codigo.
* Legibilidad del codigo.
* Upload del codigo al repositorio brindado usando git
* Se debe poder iniciar la app con solo ejecutar : npm run start
* Se debe poder iniciar la app en modo desarrollo con solo ejecutar : npm run dev

