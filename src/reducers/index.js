import { combineReducers } from 'redux';
import fieldsReducer from "./fieldsReducer";

export default combineReducers({
    fieldsReducer
});