import {LOADING, GET_INFO, CHANGE_INPUTS, GET_FIELDS, SUCCESS} from "../types/fieldsTypes";

const INITIAL_STATE={
    data: [],
    object: {},
    loading: true,
    success: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LOADING:
            return { ...state, loading: true, success: false};
        case GET_INFO:
            return { ...state,
                success:false,
                data: action.payload,
                loading: false
            };
        case GET_FIELDS:
            return {...state, object: action.payload };

        case CHANGE_INPUTS:
            return { ...state, object: action.payload};
        case SUCCESS:
            return { ...state, object: action.payload, loading: false, success: true};
        default:
            return { ...state}
    }
}