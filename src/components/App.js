import React, {Component} from 'react';
import Form from "./Form";
import { connect } from 'react-redux';
import * as fieldsActions from '../actions/fieldsActions'


class App extends Component {

    componentDidMount() {
        this.props.searchInfo();
    }

    render() {
        const { loading } = this.props;
        return (
            <div className="container margin-top">
                <div className="row">
                    <div className="col">
                        <h4 className="text-center"> Technical Evaluation Frontend UTEC </h4>
                    </div>
                </div>
                {
                    loading ?
                        <div className="row">
                            <div className="col">
                                <h6 className="text-center"> Cargando Informacion...</h6>
                            </div>
                        </div>
                        :
                        <Form/>
                }

            </div>
        )
    }
}


const mapStateToProps = ({ fieldsReducer }) => fieldsReducer;
export default connect(mapStateToProps, fieldsActions)(App);
