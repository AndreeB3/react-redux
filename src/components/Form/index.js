import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as fieldsActions from '../../actions/fieldsActions'

class Form extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isValid: true
        }
    }

    onChangeInputs = (e) => {
        e.preventDefault();
        const name = e.target.name;
        const {object} = this.props;
        const objectUpdated = {...object, [name]: e.target.value};
        this.props.onChangeObject(objectUpdated);
    };

    onSubmit = () => {
        const {object} = this.props;
        for (const prop in object) {
            if (!object[prop]) {
                this.setState({
                    isValid: false
                });
                return false;
            }
        }
        this.props.saveInfo();
    };

    renderError = () => (
        <div className="row">
            <div className="alert alert-danger">
                Por favor complete todos los campos.
            </div>
        </div>
    );
    renderSuccess = () => (
        <div className="row">
            <div className="alert alert-success">
                Los datos fueron enviados.
            </div>
        </div>
    );
    render() {
        const {data, success} = this.props;
        return (
            <div className="form">
                <div className="row">
                    <div className="col">
                        {
                            data.parameters.map((parameter) => {
                                return (
                                    <div className="form-group" key={parameter.key}>
                                        <label className="cols-sm-2 col-form">{parameter.label}</label>

                                        {
                                            parameter.type === "input" ?
                                                <input type="text" className="form-control"
                                                       name={parameter.key}
                                                       onChange={this.onChangeInputs}
                                                       value={this.props.object[parameter.key]}/>
                                                :
                                                <textarea name={parameter.key} rows={3} className="form-control"
                                                          onChange={this.onChangeInputs}
                                                          value={this.props.object[parameter.key]}/>
                                        }

                                    </div>
                                )
                            })
                        }
                    </div>
                </div>

                {
                    !this.state.isValid ?
                        this.renderError() : null
                }
                {
                    success ?
                        this.renderSuccess() : null
                }
                <div className="row">
                    <div className="col">
                        <button type="button" className="btn btn-primary" onClick={this.onSubmit}>Enviar</button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({fieldsReducer}) => fieldsReducer;
export default connect(mapStateToProps, fieldsActions)(Form);