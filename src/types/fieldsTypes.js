export const LOADING = 'LOADING';
export const GET_INFO = 'GET_INFO';
export const SUCCESS = 'SUCCESS';
export const CHANGE_INPUTS = 'CHANGE_INPUTS';
export const GET_FIELDS = 'GET_FIELDS';