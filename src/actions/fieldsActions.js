import axios from 'axios';
import {CHANGE_INPUTS, GET_FIELDS, GET_INFO, LOADING, SUCCESS} from './../types/fieldsTypes';

const URI = "http://localhost:5000";

export const searchInfo = () => async (dispatch) => {

    try {
        const response = await axios.get(`${URI}/ui/dynamic`);
        let obj= {};
        if (response.data) {
            response.data.parameters.map((parameter) => {
                obj= {...obj, [parameter.key]: ""}
            })
        }
        dispatch({
            type: GET_INFO,
            payload: response.data
        });
        dispatch({
            type: GET_FIELDS,
            payload: obj
        })
    } catch (Ex) {

    }
};

export const onChangeObject = (params) => (dispatch) => {
    dispatch({
        type: CHANGE_INPUTS,
        payload: params
    })
};



export const saveInfo = () => async (dispatch, getState) => {

    dispatch({
        type: LOADING
    });
    const {object, data} = getState().fieldsReducer;
    let parameters = [];
    let objClean = {};

    for (const prop in object) {
        parameters.push({
            key: prop,
            value: object[prop]
        });
        objClean = {...objClean, [prop]: ""}
    }
    const params = {
        idForm: data.idForm,
        parameters
    };
    const uri = data.submitEndpoint;
    try {
        const response = await axios.post(uri, params);

        if (response.data){
            dispatch({
                type: SUCCESS,
                payload: objClean
            });
        }
    } catch (e) {
        console.log(e)
    }
};